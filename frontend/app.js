const axios = require('axios');
const express = require('express');

const app = express()
const port = 3000

app.get('/', (req, res) => {
  axios.get('http://localhost:8080/status')
  .then(resp => {
    if(resp.data.status == "ok") {
      res.send("Funcionou")
    }  
  })
  .catch(error => {
    res.send("Nao funcionou")
  });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})